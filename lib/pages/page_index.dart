import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({super.key});

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {

  // 포지션의 정보를 담는 보관소
  Position? currentPosition;

  // _determinePosition 함수 호출 필요 => 페이지 들어왔을 때 실행되어야
  // 라이프사이클 initState
  @override
  void initState() {
    super.initState();
    _determinePosition();

    _determinePosition().then((value) => {
      // 디자인 요소에서 바뀐 사항 가져다 써야 되기 때문에 setState (= 원웨이바인딩)
      setState(() {
        currentPosition = value;
      })
    });
  }

  // 미래에 정보 가져다줌 => 처음에는 정보 없음 => null 허용해줘야
  // 디자인 요소에서 사용하기 때문에 정보 담은 보관소 필요
  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();

    // 위치 정보 권한 활성화되지 않았을 경우 오류 전달하기
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    // 권한 재요청하기
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }

    // 권한 영구적으로 거부
    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    // 위치 반환
    return await Geolocator.getCurrentPosition();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('테스트 앱'),
      ),
      body: Row(
        children: [
          Column(
            children: [
              Text('위도: ${currentPosition?.latitude}'),
              Text('경도: ${currentPosition?.longitude}')
            ],
          ),
        ],
      ),
    );
  }
}
